﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Snake : MonoBehaviour
{
    public int length=1;
    Vector3Int direction=Vector3Int.right;



    List<GameObject> bodys=new List<GameObject>();
    public List<Vector3Int> positions =new List<Vector3Int>();



    bool dead=false;
    public GameObject deadText;



    float timer=0f;



    public GameObject body;


    public Transform Axis;
    public Transform ThirdPersonCa;

    
    // Start is called before the first frame update
    void Start()
    {
        bodys.Add(gameObject);
        positions.Add(new Vector3Int(Config.x/2,Config.y/2,Config.z/2));
    }

    // Update is called once per frame
    void Update()
    {
        if(dead) return;
        timer+=Time.deltaTime;
        


        if(Input.GetKeyDown(KeyCode.D))
        {
            if(direction.x!=-1)
            {
                direction.Set(1,0,0);
            }
        }
        else if(Input.GetKeyDown(KeyCode.A))
        {
            if(direction.x!=1)
            {
                direction.Set(-1,0,0);
            }
        }
        else if(Input.GetKeyDown(KeyCode.LeftShift))
        {
            if(direction.y!=-1)
            {
                direction.Set(0,1,0);
            }
        }
        else if(Input.GetKeyDown(KeyCode.Space))
        {
            if(direction.y!=1)
            {
                direction.Set(0,-1,0);
            }
        }
        else if(Input.GetKeyDown(KeyCode.W))
        {
            if(direction.z!=-1)
            {
                direction.Set(0,0,1);
            }
        }
        else if(Input.GetKeyDown(KeyCode.S))
        {
            if(direction.z!=1)
            {
                direction.Set(0,0,-1);
            }
        }

        if(timer<0.7f) return;      //每0.7移動一步，將Direct代入
        timer=0;
        
        


        if(length>bodys.Count)
        {
            GameObject go=Instantiate(body,bodys[positions.Count-1].transform.position,Quaternion.identity); //下一格就會往前
            bodys.Add(go);
            positions.Add(Vector3Int.zero);
        }


        for(int i=bodys.Count-1;i>0;--i)
        {
            bodys[i].transform.DOMove(bodys[i-1].transform.position,0.2f);
            positions[i]=positions[i-1];
        }


        bodys[0].transform.DOMove(new Vector3(bodys[0].transform.position.x+direction.x*Config.gridSize,bodys[0].transform.position.y+direction.y*Config.gridSize,bodys[0].transform.position.z+direction.z*Config.gridSize),0.2f);//移動時間
        positions[0]+=direction;
        Axis.DOMove(new Vector3(Axis.transform.position.x+direction.x*Config.gridSize,Axis.transform.position.y+direction.y*Config.gridSize,Axis.transform.position.z+direction.z*Config.gridSize),0.2f);
        ThirdPersonCa.DOMove(new Vector3(ThirdPersonCa.transform.position.x+direction.x*Config.gridSize,ThirdPersonCa.transform.position.y+direction.y*Config.gridSize,ThirdPersonCa.transform.position.z+direction.z*Config.gridSize),0.2f);


        for(int i=1;i<positions.Count;++i)
        {
            if(positions[0]==positions[i])
            {
                dead=true;
                deadText.SetActive(true);
                break;
            }
        }


        if(positions[0].x>=Config.x||positions[0].x<0||positions[0].y>=Config.y||positions[0].y<0||positions[0].z>=Config.z||positions[0].z<0)
        {
            dead=true;
            deadText.SetActive(true);
        }
    }



    public void AddLength()
    {
        length=bodys.Count+1;
    }



    public bool SnakeInGrid(Vector3Int position)
    {
        for(int i=0;i<positions.Count;++i)
        {
            if(positions[i]==position)
            {
                return true;
            }
        }
        return false;
    }
}
