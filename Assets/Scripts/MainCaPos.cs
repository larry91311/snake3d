﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCaPos : MonoBehaviour
{
    public Transform playerTran;
    public Vector3 offSet;
    // Start is called before the first frame update
    void Start()
    {
        transform.position=playerTran.position+offSet;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
