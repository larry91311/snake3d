﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCam : MonoBehaviour
{
   public float x;
   public float y;
   public float xSpeed=1;  //x靈敏度
   public float ySpeed=1;  //y靈敏度
   private Quaternion roatationEuler;

   private void LateUpdate() {
    x+=Input.GetAxis("Mouse X")*xSpeed*Time.deltaTime;
    y-=Input.GetAxis("Mouse Y")*ySpeed*Time.deltaTime;


    if(x>360)
    {
        x-=360;
    }
    else if(x<0)
    {
        x+=360;
    }

    roatationEuler=Quaternion.Euler(y,x,0);
    transform.rotation=roatationEuler;
   }

}
