﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotate : MonoBehaviour
{
    private float x;
    private float y;

    public  float xRotSpeed=5.0f;
    public  float yRotSpeed=5.0f;
    private Vector3 rotateValue;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        y = Input.GetAxis("Mouse X")*xRotSpeed;
        x = Input.GetAxis("Mouse Y")*yRotSpeed;
        rotateValue = new Vector3(x, y * -1, 0);
        
        transform.eulerAngles = transform.eulerAngles - rotateValue;
    }
}
