﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    float rotateX=0;   //上下
    
    float rotateZ=0;    //左右

    Vector3 rotateVector;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        rotateZ=0;
        rotateX=0;
        if(Input.GetKey(KeyCode.UpArrow))
        {
            
            rotateX++;
        }

        if(Input.GetKey(KeyCode.DownArrow))
        {
            
            rotateX--;
        }

        if(Input.GetKey(KeyCode.RightArrow))
        {
            rotateZ++;
        }

        if(Input.GetKey(KeyCode.LeftArrow))
        {
            rotateZ--;
        }

        
        rotateVector+=new Vector3(rotateX,0,rotateZ);
        Quaternion rotation = Quaternion.LookRotation(rotateVector);
        transform.rotation=rotation;
        
    }
}
