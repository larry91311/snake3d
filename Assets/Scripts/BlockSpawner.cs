﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSpawner : MonoBehaviour
{
    public GameObject floorPre;
    public GameObject OutsideBlock;

    public int energySpawnTime=5;
    float timer=0;
    public Snake snake;
    public GameObject energy;

    // Start is called before the first frame update
    void Start()
    {
        for(int i=0;i<Config.x;i++)
        {
            for(int j=0;j<Config.y;j++)
            {
                for(int z=0;z<Config.z;z++)
                {
                    if(i==Config.x-1||j==Config.y-1||z==Config.z-1||i==0||j==0||z==0)
                    {
                        Instantiate(OutsideBlock,new Vector3(-Config.x/2+i,-Config.y/2+j,-Config.z/2+z),Quaternion.identity);
                    }
                    else 
                    {
                        Instantiate(floorPre,new Vector3(-Config.x/2+i,-Config.y/2+j,-Config.z/2+z),Quaternion.identity);
                    }           
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        timer+=Time.deltaTime;
        if(timer<energySpawnTime) return;
        timer=0;
        CreateEnergy();
    }

    void CreateEnergy()
    {
        Vector3Int position=new Vector3Int(Random.Range(0,Config.x),Random.Range(0,Config.y),Random.Range(0,Config.z));
        while(snake.SnakeInGrid(position))      
        {
            position=new Vector3Int(Random.Range(0,Config.x),Random.Range(0,Config.y),Random.Range(0,Config.z));
            
        }
        Instantiate(energy,new Vector3((position.x-Config.x/2)*Config.gridSize,(position.y-Config.y/2)*Config.gridSize,(position.x-Config.x/2)*Config.gridSize),Quaternion.identity);
    }
}
